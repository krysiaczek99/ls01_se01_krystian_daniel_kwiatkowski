package UI;

import javax.swing.*;
import javax.swing.plaf.ColorChooserUI;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class FormAendernNach {

    private JButton btnExit;
    private JButton btnLinksbundig;
    private JButton btnZentriert;
    private JButton btnRechts;
    private JButton btnSchriftPlus;
    private JButton btnSchriftRot;
    private JButton btnWriteLabel;
    private JTextField textInput;
    private JButton btnSchriftMinus;
    private JButton btnSchriftBlau;
    private JButton btnSchriftSchwarz;
    private JButton btnArial;
    private JButton btnHinterGelb;
    private JButton btnHinterRot;
    private JButton btnComicSansMS;
    private JButton btnCourierNew;
    private JButton btnHinterGruen;
    private JButton btnHinterBlau;
    private JButton btnHinterStandard;
    private JButton btnHinterWaehlen;
    private JPanel panel1;
    private JButton btnClearLabel;
    private JLabel textOut;

    public FormAendernNach() {
        /**
         * Task 1
         */
        btnHinterRot.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                panel1.setBackground(Color.RED);
            }
        });
        btnHinterGruen.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                panel1.setBackground(Color.GREEN);
            }
        });
        btnHinterBlau.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                panel1.setBackground(Color.BLUE);
            }
        });
        btnHinterGelb.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                panel1.setBackground(Color.YELLOW);
            }
        });
        btnHinterStandard.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                panel1.setBackground(UIManager.getColor("Panel.background"));
            }
        });
        btnHinterWaehlen.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                Color choose = JColorChooser.showDialog(null, "Wähle Hintergrundfarbe:", null);
                if (choose != null) {
                    panel1.setBackground(choose);
                }
            }
        });

        /**
         * Task 2
         */
        btnCourierNew.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                textOut.setFont(new Font("Courier New", 0, textInput.getFont().getSize()));
            }
        });
        btnComicSansMS.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                textOut.setFont(new Font("Comic Sans MS", 0, textInput.getFont().getSize()));
            }
        });
        btnArial.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                textOut.setFont(new Font("Arial", 0, textInput.getFont().getSize()));
            }
        });
        /**
         * Task 3
         */
        btnClearLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                textOut.setText("");
            }
        });
        btnWriteLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                textOut.setText(textInput.getText());
            }
        });

        /**
         * Task 4
         */
        btnSchriftRot.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                textOut.setForeground(Color.RED);
            }
        });
        btnSchriftBlau.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                textOut.setForeground(Color.BLUE);
            }
        });
        btnSchriftSchwarz.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                textOut.setForeground(Color.BLACK);
            }
        });
        /**
         * Task 5
         */
        btnSchriftPlus.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                int size = textOut.getFont().getSize();
                textOut.setFont(new Font("DEFAULT", 0, ++size));
            }
        });
        btnSchriftMinus.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                int size = textOut.getFont().getSize();
                textOut.setFont(new Font("DEFAULT", 0, --size));
            }
        });
        /**
         * Task 6
         */
        btnLinksbundig.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                textOut.setHorizontalAlignment(JLabel.LEFT);
            }
        });
        btnZentriert.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                textOut.setHorizontalAlignment(JLabel.CENTER);
            }
        });
        btnRechts.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                textOut.setHorizontalAlignment(JLabel.RIGHT);
            }
        });

        /**
         * Task 7
         * ToDo: check for another possibility to close the instance of programm
         */
        btnExit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                System.exit(0);
            }
        });
    }

    public void show () {
        JFrame frame = new JFrame("Form Ändern");
        frame.setContentPane(new FormAendernNach().panel1);
        frame.setVisible(true);
    }
}
