package temp_abgabe;
import java.util.ArrayList;
import temp_abgabe.Temperatur;


public class Main {
	public static void main(String[] args) {
		ArrayList <String> names = new ArrayList <String> ();
		ArrayList <Double> tempi = new ArrayList <Double> ();
		
		names.add("Fahrenheit");
		names.add("Clesius");
		tempi.add(-20.0);
		tempi.add(-10.0);
		tempi.add(0.0);
		tempi.add(20.0);
		tempi.add(30.0);
		
		Temperatur temp = new Temperatur ();
			temp.showTable(names, tempi);
	}

}
