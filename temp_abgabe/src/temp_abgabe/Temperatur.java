package temp_abgabe;
import java.util.ArrayList;

public class Temperatur {
	public void showTable(ArrayList<String> tableHeader, ArrayList<Double> tempValues) {
		double cel = 0;
		System.out.printf("%-12s | %10s \n", tableHeader.get(0), tableHeader.get(1));
		System.out.println("----------------------");
		for (int i = 1; i < tempValues.size(); ++i) {
			cel = (tempValues.get(i) - 32) * 5/9;
			System.out.printf("%+-12.2f | %10.2f \n", tempValues.get(i), cel);
		}
	}
}
