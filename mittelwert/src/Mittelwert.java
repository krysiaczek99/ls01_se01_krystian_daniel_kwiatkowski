import java.util.Scanner;

public class Mittelwert {
	public static Scanner tastatur = new Scanner(System.in);

	public static double avarage () {
		double sum = 0;
		String input;
		do {
			input = tastatur.nextLine();
			try {
				sum += Double.parseDouble(input);
			} catch (Exception e) {}
		} while (!input.isBlank());
		return (sum/ 2.0);
	}

	public static void output (double a, double b, double avarage) {
		System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", a, b, avarage);
	}

   public static void main(String[] args) {
      double x = 2.0;
      double y = 4.0;
      output (x, y, avarage ());
   }
}
