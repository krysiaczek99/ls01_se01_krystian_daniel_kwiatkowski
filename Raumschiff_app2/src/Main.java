import ladungPackage.Ladung;
import raumschiffPackage.*;


public class Main {
    public static void main(String[] args){
        Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Heighta");
        klingonen.addLadung(new Ladung("Ferengi Schneckensaft", 200));
        klingonen.addLadung(new Ladung("Batleth Klingonen Schwert", 200));

        Raumschiff romulaner = new Raumschiff(2, 50, 100, 100, 100, 2, "IRW Khazara");
        romulaner.addLadung(new Ladung("Borg-Schrott", 5));
        romulaner.addLadung(new Ladung("Rote Materie", 2));
        romulaner.addLadung(new Ladung("Plasma-Waffe", 50));

        Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "NlVar");
        vulkanier.addLadung(new Ladung("Forschungssonde", 35));
        vulkanier.addLadung(new Ladung("Photonentorpedo", 3));

        klingonen.torpedosAbschiessen(romulaner);
        romulaner.torpedosAbschiessen(klingonen);
        vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
        klingonen.zustandDesRaumschiffs();
        klingonen.ladungsverzeichnisAusgeben();

        vulkanier.reparaturDurchfuehren(vulkanier.getSchildeInProzent() < 70,
                                        vulkanier.getEnergieversorgungInProzent() < 70,
                                        vulkanier.getHuelleInProzent() < 70,
                                        vulkanier.getAndroidenAnzahl());

        vulkanier.photonentorpedosLaden(vulkanier.getLadungsverzeichnis().stream().filter(x -> x.getBezeichnung().equals("Photonentorpedo")).findFirst().get().getMenge());

        vulkanier.ladungsverzeichnisAufraeumen();
        klingonen.torpedosAbschiessen(romulaner); klingonen.torpedosAbschiessen(romulaner);

        klingonen.zustandDesRaumschiffs();
        klingonen.ladungsverzeichnisAusgeben();

        romulaner.zustandDesRaumschiffs();
        romulaner.ladungsverzeichnisAusgeben();

        Raumschiff.boradcastKommunikatorAnzeigen();
    }
}
