package sterne;
import java.io.PrintStream;
import java.util.*;

public class Main {
	public static void stern () {
		System.out.printf("%10s", "**\n");
		System.out.printf("%s", "*");
		System.out.printf("%20s", "*\n");
		System.out.printf("%s", "*\n");
		System.out.printf("%20S", "*\n");
		System.out.printf("%10S", "**");
	}
	public static void newLine () {
		System.out.print("\n");
	}
	public static void fac (int n) {
		int k = 1;
		for (int i = 1; i <= n; ++i) {
			k *= i;
			System.out.printf("%d! %5s, %5d", i, "=", k); 
			System.out.print("\n");
		}
	}
	public static void temp (ArrayList<String> name, ArrayList<Double> fah) {
		System.out.printf("%s %s %s \n", name.get(0), "|", name.get(1));
		double cel = 0;
		for (int i = 1; i < fah.size(); ++i) {
			cel = (fah.get(i) - 32) * 5/9;
			System.out.printf("%+f", fah.get(i));
		}
	}
}
