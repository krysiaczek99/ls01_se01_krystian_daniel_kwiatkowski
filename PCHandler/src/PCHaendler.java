import java.util.Scanner;

public class PCHaendler {

	static Scanner tastatur = new Scanner(System.in);
	
	public static String readString (String text) {
		System.out.println(text);
		return tastatur.next();
	}
	
	public static int readInt (String text) {
		System.out.println(text);
		return tastatur.nextInt();
	}
	
	public static double readDouble (String text) {
		System.out.println(text);
		return tastatur.nextDouble();
	}
	private static double netto(int anzahl, double preis) {
		return anzahl  * preis;
	}
	private static double brutto(double netto, double mwst) {
		return netto * (1 + mwst / 100);
	}
	
	public static void output (String artikel, int anzahl, double netto, double brutto, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, netto);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, brutto, mwst, "%");
	}
	
	public static void main(String[] args) {
		String artikel = readString("was m�chten Sie bestellen?");
		int anzahl = readInt("Geben sie die Anzahl an.");
		double preis = readDouble("Geben Sie den Nettopreis ein:");
		double mwst = readDouble ("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
			
		output (artikel, anzahl, netto (anzahl, preis), brutto (netto (anzahl, preis), mwst), mwst);
	}

}
