import java.util.*;

public class ArrayHelper {
    static Scanner myScanner = new Scanner(System.in);
    /*
        * Aufgabe 1
    */
    public static String convertArrayToString(int[] zahlen) {
        String zahlenString = "";
        for (int i : zahlen) {
            if (i > zahlen.length-1) {
                zahlenString += i + ", ";
            } else {
                zahlenString += i;
            }
        }
        return zahlenString;
    }
    /*
        * Aufgabe 2
    */
    public static void converArray (int [] zahlen) {
        int k = 0;
        int l = zahlen.length;
        for (int i = 0; i < zahlen.length/2; ++i) {
            k = zahlen[i];
            zahlen[i] = zahlen.length - (i);
            zahlen[zahlen.length - (i + 1)] = k;
        }
        for (int i : zahlen) {
            System.out.print(i);
        }
    }
    /*
        * Aufgabe 3
    */
    public static int [] converArrayWithArray (int [] zahlen) {
        int [] newArray = new int [zahlen.length];
        for (int i = zahlen.length - 1; i > -1; --i) {
            newArray [newArray.length - i-1] = zahlen[i];
        }
        return newArray;
    }
    /*
        * Aufgabe 3
    */
    public static double [] [] tempConverter (int numOfTemps) {
        double [] [] temps = new double[numOfTemps] [2];
        double k = 10.0;
        for (int i = 0; i < numOfTemps; ++ i) {
            temps[i][0] = k;
            k += 10.0;
        }
        for (int i = 0; i < numOfTemps; ++ i) {
            temps[i][1] = 5/9 * (temps[i][0]-32);
        }
        return temps;
    }

    public static int [] [] returnMatrix (int columns, int rows) {
        int [] [] matrix = new int [columns] [rows];
        for (int i = 0; i < matrix.length; ++ i) {
            for (int j = 0; j < matrix[i].length; ++ j) {
                matrix[i][j] = myScanner.nextInt();
            }
        }
        return matrix;
    }
    public static boolean transposedMatrix (int [] [] matrix) {
        if (matrix.length == matrix[0].length) {
            for (int i = 0; i < matrix.length; ++ i) {
                for (int j = 0; j < matrix.length; ++ j) {
                    if (matrix[i][j] != matrix[j][i]) {
                        return false;
                    }
                }
            }
        } else {
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.print(transposedMatrix(returnMatrix(2,2)));
    }
}
