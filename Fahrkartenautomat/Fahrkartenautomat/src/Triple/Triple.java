package Triple;

public class Triple<A, B, C> {
    private A first;
    private B second;
    private C third;

    public Triple (A first, B second, C third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public A first() { return first; }
    public B second() { return second; }
    public C third() { return third; }

    public boolean first(A newValue) {
        try {
            first = newValue;
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public boolean second(B newValue) {
        try {
            second = newValue;
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public boolean third(C newValue) {
        try {
            third = newValue;
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    @Override
    public String toString () {
        return (first + " - " + second + " - " + third);
    }
    @Override
    public boolean equals (Object obj) {
        if (!(obj instanceof Triple)) {
            return false;
        }
        Triple<?, ?, ?> tripleInstance = (Triple<?, ?, ?>) obj;
        return first.equals(tripleInstance.first) && second.equals(tripleInstance.second) && third.equals(tripleInstance.third);
    }
}
