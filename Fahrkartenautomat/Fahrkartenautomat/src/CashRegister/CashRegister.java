package CashRegister;

import Triple.Triple;

import java.util.Arrays;
import java.util.List;

public class CashRegister {
    private final List<Triple<Integer, Integer, Integer>> moneyState = Arrays.asList(
                new Triple (5000, 50, 100),
                new Triple (2000, 50, 100),
                new Triple (1000, 50, 100),
                new Triple (500, 50, 100),
                new Triple (200, 50, 100),
                new Triple (100, 50, 100),
                new Triple (50, 50, 100),
                new Triple (20, 50, 100),
                new Triple (10, 50, 100),
                new Triple (5, 50, 100)
        );
    public String euroCent (int cash) {
        return cash > 100 ? "Euro" : "Cent";
    }
    public void status () {
        System.out.println("aktuelle Bargegenstände:");
        System.out.println("€" + "-" + "Anzahl der Münzen" + "-" + "% der max.");
        for (int i = 0; i < moneyState.size()-1; ++i) {
            System.out.println(moneyState.get(i).first() + euroCent (moneyState.get(i).first()) +
                                "-" + moneyState.get(i).second()  + "-" +
                                 (double)(moneyState.get(i).third()/100)+ "%");
        }
    }
    public boolean checkStatus (int cashIn) {
        if (moneyState.get(moneyState.indexOf(cashIn)).second() >= moneyState.get(moneyState.indexOf(cashIn)).third()) {
            return true;
        }
        return false;
    }
    public void cashIn (int cash) {
        if (checkStatus(cash) == true) {
            moneyState.get(moneyState.indexOf(cash)).second(moneyState.get(moneyState.indexOf(cash)).second()+1);
        }
    }
    public void graphicBanknote (int cash) {
        for (int i = 0; i < 22; ++i) {
            System.out.print("*");
        }
        System.out.println("");
        for (int i = 0; i < 1; ++i) {
            System.out.printf("%s %20s\n", "*", "*");
        }
        System.out.printf("%s %8d %s %6s\n", "*", cash/100, "Euro", "*");
        for (int i = 0; i < 1; ++i) {
            System.out.printf("%s %20s\n", "*", "*");
        }
        for (int i = 0; i < 22; ++i) {
            System.out.print("*");
        }
    }
    public void graphiceCoins (int cash) {
        int r = 30, pr = 2, n = 0;
        for (int i = -r; i <= r; ++i) {
            for (int j = -r; j <= r; j++) {
                double d = ((i*pr)/r)*((i*pr)/r) + (j/r)*(j/r);
                if (d > 0.95 && d < 1.08)  {
                    System.out.printf("*");
                } else {
                    System.out.printf(" ");
                }
            }
            System.out.printf("\n");
        }
    }
    public void graphicCash (int cash) {
        if (cash > 499) {
            graphicBanknote(cash);
        } else {
            graphiceCoins(cash);
        }
    }
}
