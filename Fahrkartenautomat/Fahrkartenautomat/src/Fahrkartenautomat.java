import java.util.*;
import Triple.*;
import Ticket.*;
import CashRegister.*;

class Fahrkartenautomat
{
	final private static List <Triple<Integer, String, Double>> cardTable = Arrays.asList(
			new Triple(1, "Einzelfahrschein Berlin AB", 2.9),
			new Triple(2, "Einzelfahrschein Berlin BC", 3.3),
			new Triple(3, "Einzelfahrschein Berlin ABC", 3.6),
			new Triple(4, "Kurzstrecke", 1.9),
			new Triple(5, "Tageskarte Berlin AB", 8.6),
			new Triple(6, "Tageskarte Berlin BC", 9.0),
			new Triple(7, "Tageskarte Berlin ABC", 9.6),
			new Triple(8, "Kleingruppen-Tageskarte Berlin AB", 23.5),
			new Triple(9, "Kleingruppen-Tageskarte Berlin BC", 24.3),
			new Triple(10, "Kleingruppen-Tageskarte Berlin ABC", 24.9)
		);
	private static Ticket vbbTickets = new Ticket(cardTable);

	static Scanner tastatur = new Scanner(System.in);
	static ArrayList<Integer> alleMunzen = new ArrayList<Integer>(Arrays.asList(5000, 2000, 1000, 500, 200, 100, 50, 20, 10, 5));

	public static void ticketOrServiceMenu () {
		System.out.println("1. Ticketkaufen");
		System.out.println("2. Service");
		System.out.println("0. Beenden");
		System.out.print("Ihrer Wahl: ");
	}
	public static int ticketOrService () {
		return tastatur.nextInt();
	}
	public static void service () {
        int NumWrongPass = 0;
        do {
            //pas_input_bool_set
        } while (NumWrongPass < 3);

	}

	public static void automat () {
		int zuZahlenderBetrag = 0;
		int eingezahlterGesamtbetrag = 0;
		int rueckgabebetrag = 0;
		int ticketsNumber;

		int ticketOrService = 0;
		int ticket = 0;

		do {
			ticketOrServiceMenu ();
			ticketOrService = ticketOrService();
			if (ticketOrService == 1) {
				do {
					ticketMenu ();
					ticket = ticketChoice();
					if (ticket < vbbTickets.size()+1 && ticket > 0) {
						ticketsNumber = getNumberOfTickets();
						zuZahlenderBetrag += (int) ((vbbTickets.get(ticket-1).third() * 100)*ticketsNumber);
					}
				} while (ticket != 0);
				System.out.print(zuZahlenderBetrag);
				eingezahlterGesamtbetrag = fahrkartenBezahlen (eingezahlterGesamtbetrag, zuZahlenderBetrag);

				ticketPrinting();
				rueckgabebetrag = (int)((eingezahlterGesamtbetrag - zuZahlenderBetrag));
				for (int i = 0; i < alleMunzen.size(); ++i) {
					rueckgabebetrag = rueckgeldAusgeben (rueckgabebetrag, alleMunzen.get(i));
				}
				endInformation();
			} else {
				service ();
			}
		} while ((ticketOrService >= 1));

	}

	public static void ticketMenu () {
		vbbTickets.list();
		System.out.println("0. Bezahlen");
		System.out.println("Ihrer Wahl");
	}
	public static int ticketChoice () {
		return tastatur.nextInt();
	}

	public static int getNumberOfTickets() {
		int ticketsNumber;
		do {
			System.out.print("Wie viel Tickets wollen Sie haben? ");
			ticketsNumber = tastatur.nextInt();
		} while (ticketsNumber < 0 && ticketsNumber < 11);
		return ticketsNumber;
	}

	public static int fahrkartenBezahlen (int eingezahlterGesamtbetrag, int zuZahlenderBetrag) {
		while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.printf("%s %.2f \n","Noch zu zahlen: ", ((double)(zuZahlenderBetrag - eingezahlterGesamtbetrag))/100);
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 50 Euro): ");
	    	   double eingeworfeneMunze = (int)tastatur.nextDouble()*100;
	           eingezahlterGesamtbetrag += eingeworfeneMunze;
	       }
		return eingezahlterGesamtbetrag;
	}

	public static void sleep (int milisekunde) {
		try {
			Thread.sleep(milisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void ticketPrinting () {
		System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          sleep(250);
	       }
	       System.out.println("\n\n");
	}


	public static void endInformation () {
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt.");
	}

	public static int rueckgeldAusgeben (int rueckgabebetrag, int munze) {
		if(rueckgabebetrag >= 0) {
	 	   while(rueckgabebetrag >= munze)
	        {
		          System.out.printf("%s %.2f %s \n", "Der Rückgabebetrag in Höhe von ", (double)rueckgabebetrag/100, " EURO");
		          rueckgabebetrag -= munze;
		          if (rueckgabebetrag + munze > 99) {
		        	  System.out.println((munze/100) + " EURO");
		          } else {
		        	  System.out.println(munze + " CENT");
		          }
	        }
		}
 	   return rueckgabebetrag;
    }
    public static int iterator = 0;
    public static int returnRekursion (int rueckgabebetrag) {
        if (rueckgabebetrag <= 0) {
            return 1;
        } else {
            while(rueckgabebetrag >= alleMunzen.get(iterator))
	        {
		          System.out.printf("%s %.2f %s \n", "Der Rückgabebetrag in Höhe von ", ((double)rueckgabebetrag)/100, " EURO");
		          rueckgabebetrag -= alleMunzen.get(iterator);
		          if (rueckgabebetrag + alleMunzen.get(iterator) > 99) {
		        	  System.out.println((alleMunzen.get(iterator)/100) + " EURO");
		          } else {
		        	  System.out.println(alleMunzen.get(iterator) + " CENT");
		          }
	        }
            iterator++;
        }
        return returnRekursion (rueckgabebetrag);
    }

    public static void main(String[] args)
    {
    	automat();
    }
}
