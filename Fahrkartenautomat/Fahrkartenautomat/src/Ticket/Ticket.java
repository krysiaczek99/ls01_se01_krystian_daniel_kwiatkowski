package Ticket;
import java.util.*;
import Triple.Triple;
import java.lang.*;

public class Ticket {
    private List<Triple<Integer, String, Double>> tickets = new ArrayList<Triple<Integer, String, Double>> ();

    public Ticket (List<Triple<Integer, String, Double>> init) {
        this.tickets.addAll(init);
    }
    public void add (Integer id, String name, Double price) {
        int k = 0;
        Triple<Integer, String, Double> newTicket = new Triple<Integer, String, Double> (id, name, price);
        boolean equal = false;
        for (Triple<Integer, String, Double> i : this.tickets) {
            if (this.tickets.get(k).equals(newTicket)) {
                equal =  true;
            }
            ++k;
        }
        if (equal == false) {
            this.tickets.add(newTicket);
        }
    }
    public void add (Triple<Integer, String, Double> newTicket) {
        this.add(newTicket.first(), newTicket.second(), newTicket.third());
    }
    public void add (List<Triple<Integer, String, Double>> newTickets) {
        for (Triple<Integer, String, Double> n : newTickets) {
            add(n);
        }
    }
    public Triple<Integer, String, Double> get (int position) {
        return this.tickets.get(position);
    }
    public int size () {
        return this.tickets.size();
    }

    public void remove () {
        this.tickets.remove(this.tickets.size() - 1);
    }
    public void remove (int ... position) {
        for (int i : position) {
            this.tickets.remove(i);
        }
    }
    public void remove (Triple<Integer, String, Double> ... ticket) {
        for (Triple<Integer, String, Double> i : ticket) {
            if (this.tickets.equals(i)) {
                this.tickets.remove(i);
            }
        }
    }

    public void list () {
        for (Triple<Integer, String, Double> i : tickets) {
            System.out.println(i.toString());
        }
    }
}
