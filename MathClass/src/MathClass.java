import java.util.*;

public class MathClass {
	public static <T extends Comparable<T>> T minimum(List<T> numbers) {
		T a = numbers.get(0);
		
		for (int i = 1; i < numbers.size(); ++i) {
			if (numbers.get(i) < a) {
				a = numbers.get(i);
			}
		}
		
		return a;
	}
	public static void main( String[] args ) {
	}	
}
