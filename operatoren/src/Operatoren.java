﻿public class Operatoren {
  public static void main(String [] args){
    /* 1. Vereinbaren Sie zwei Ganzzahlen.*/

    System.out.println("UEBUNG ZU OPERATOREN IN JAVA\n");
    /* 2. Weisen Sie den Ganzzahlen die Werte 75 und 23 zu
          und geben Sie sie auf dem Bildschirm aus. */
          int number1 = 75;
    	  int number2 = 23;
    	  System.out.println("Zahl 1: " + number1 + " Zahl 2: " + number2);

    /* 3. Addieren Sie die Ganzzahlen
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
    	  int sum = number1 + number2;
    	  System.out.println("Summe: " + sum);

    /* 4. Wenden Sie alle anderen arithmetischen Operatoren auf die
          Ganzzahlen an und geben Sie das Ergebnis jeweils auf dem
          Bildschirm aus. */
    	  int minus = number1 - number2;
    	  System.out.println("Summe: " + minus);
    	  int mult = number1 * number2;
    	  System.out.println("Summe: " + mult);
    	  int sub = number1 / number2;
    	  System.out.println("Summe: " + sub);
    	  int mod = number1 % number2;
    	  System.out.println("Summe: " + mod);

    /* 5. Ueberprüfen Sie, ob die beiden Ganzzahlen gleich sind
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
    	  boolean same = (number1 == number2) ? true : false;
    	  System.out.println("Gleich: " + same);

    /* 6. Wenden Sie drei anderen Vergleichsoperatoren auf die Ganzzahlen an
          und geben Sie das Ergebnis jeweils auf dem Bildschirm aus. */
    	  boolean bigger = (number1 > number2) ? true : false;
    	  System.out.println("Gleich: " + bigger);
    	  boolean less = (number1 < number2) ? true : false;
    	  System.out.println("Gleich: " + less);
    	  boolean notEqual = (number1 != number2) ? true : false;
    	  System.out.println("Gleich: " + notEqual);

    /* 7. Ueberprüfen Sie, ob die beiden Ganzzahlen im  Interval [0;50] liegen
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
    	  boolean interval = ((number1 < 50 && number1 > 0)&&(number2 < 50 && number2 > 0)) ? true : false;
    	  System.out.println("Gleich: " + interval);
          
  }//main
}// Operatoren
