import java.util.Scanner;
public class while_loop {
	static Scanner tast = new Scanner(System.in);
	
	public static void main(String[] args) {
		/*
		 * Aufgabe 1 -> b
		 */
		int input;
		int n = 1;
		input = tast.nextInt();
		
		while (input >= 1) {
			System.out.print(n + ", ");
			--input;
			if (input == 1) {
				System.out.print(n);
			}
		}
		/*
		 * Aufgabe 1 -> a
		 */
		input = tast.nextInt();
		while (n <= input) {
			System.out.print(n + ", ");
			if (n == input) {
				System.out.print(n);
			}
			++n;
		}
		/*
		 * Aufgabe 2
		 */
		n=1;
		input = tast.nextInt();
		int fak = 1;
		if (input <= 20) {
			if (input == 0) {
				System.out.println(1);
			} else {
				while (n < input) {
					fak *= n;
					++n;
				}
				System.out.println(fak);
			}
		}
		/*
		 * Aufgabe 3
		 */
		n=0;
		int sum = 0;
		String number = "";
		number = tast.next();
		while (n < number.length()) {
			sum += (int)number.indexOf(n);
			++n;
		}
		System.out.println(sum);
		
		/*
		 * Aufgabe 4
		 */
		System.out.print("Bitte den Startwert in Celsius eingeben: ");
			int startValue = tast.nextInt();
		System.out.print("Bitte den Endwert in Celsius eingeben: ");
			int endValue = tast.nextInt();
		System.out.print("Bitte den Schrittwert in Celsius eingeben: ");
			int stepValue = tast.nextInt();
			
		while (startValue <= endValue) {
			System.out.printf("%.2d %5s %.2d %n",startValue, "", (32 * 9/5 + 32) );
			startValue += stepValue;
		}
		/*
		 * Aufgabe 5
		 */	
		System.out.print("Laufzeit (in Jahren) des Sparvertrags: ");
			int runtime = tast.nextInt();
		System.out.print("Wie viel Kapital (in Euro) möchten Sie anlegen: ");
			int cash = tast.nextInt();
			startValue = cash;
		System.out.print("Bitte den Schrittwert in Celsius eingeben: ");
			double zins = tast.nextDouble();
			
		n = 1;
		while (n <= runtime) {
			cash *= zins;
		}
		System.out.print("Eingezahltes Kapital: " + startValue);
		System.out.print("Ausgezahltes Kapital: " + cash);
		
		
	}
}
